# PHP and MongoDB for Bitbucket Pipelines

This repository contains a Dockerfile as well as a simple example that shows how you can run your own Docker container with PHP and MongoDB on Bitbucket Pipelines.

The Docker image is using PHP 7.1.1 and MongoDB 3.2.

## Quickstart

### Using the image with Bitbucket Pipelines

Just copy/paste the YML below in your bitbucket-pipelines.yml and adapt the script to your needs.

```yaml
# This is a sample build configuration for PHP.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Dockerhub as your build environment.
image: spittet/php-mongodb

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - service mongod start
          - composer require mongodb/mongodb --ignore-platform-reqs
          - php test.php
```

### Using this in a script

You'll find a sample script in this repository in test.php. It simply connects to MongoDB and then lists the existing databases.

```php
<?php
require 'vendor/autoload.php'; // include Composer's autoloader

$client = new MongoDB\Client("mongodb://localhost:27017");
foreach ($client->listDatabases() as $databaseInfo) {
    var_dump($databaseInfo);
}
```

## Create your own image

If you want to use a different version of PHP you can simply create your own image for it. Just copy the content of the Dockerfile and replace the first line.

This image is built from the official PHP image at https://hub.docker.com/_/php/ and you can find there all the different versions that are supported.

Your Dockerfile won't need to have an ENTRYPOINT or CMD line as Bitbucket Pipelines will run the script commands that you put in your bitbucket-pipelines.yml file instead.

```
FROM php:7.1
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 \
  && echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list \
  && apt-get update \ 
  && apt-get install -y mongodb-org --no-install-recommends \
  && apt-get install -y libssl-dev unzip \
  && pecl install mongodb \
  && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && docker-php-ext-enable mongodb \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# This Dockerfile doesn't need to have an entrypoint and a command
# as Bitbucket Pipelines will overwrite it with a bash script.
```

### Build the image

```bash
docker build -t <your-docker-account>/php-mongodb .
```

### Run the image locally with bash to make some tests

```bash
docker run -i -t <your-docker-account>/php-mongodb /bin/bash
```

### Push the image back to the Docker Hub

```bash
docker push <your-docker-account>/php-mongodb
```
